FROM python:3.8.5-alpine3.12

ENV NEO4J_URI="bolt://neo4j:7687" \
    PYTHONUNBUFFERED="1" \
    GOOGLE_APPLICATION_CREDENTIALS=/cartography/persona-web-e3fb26cbe504.json

COPY requirements.txt /cartography/requirements.txt
RUN /usr/local/bin/pip install --no-cache-dir --requirement /cartography/requirements.txt

COPY persona-web-e3fb26cbe504.json /cartography/persona-web-e3fb26cbe504.json
COPY docker-entrypoint.sh /cartography/docker-entrypoint.sh
RUN chmod +x /cartography/docker-entrypoint.sh

ENTRYPOINT ["/cartography/docker-entrypoint.sh"]
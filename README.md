# Persona GCP Cartography

A Docker image for using [Lyft's Cartography tool](https://github.com/lyft/cartography) to understand our GCP environment. Adapted from https://github.com/williamjacksn/docker-cartography.

## Usage

You'll need some GCP privileges for this to be useful. In particular, per [Lyft's GCP setup guide](https://github.com/lyft/cartography/blob/master/docs/setup/config/gcp.md), you'll need:

- `roles.iam.securityReviewer`

- `roles/resourcemanager.organizationViewer`

- `roles/resourcemanager.folderViewer`

Replaces instances of `persona-web-e3fb26cbe504.json` with your own GCP credentials file, and place the file in the root of this repo.

Then, do a 
```sh
docker-compose build
```
followed by
```sh
docker-compose up -d neo4j
```
and then
```
docker-compose up cartography
```

Once that finishes, visit http://localhost:7474/, and poke around!
